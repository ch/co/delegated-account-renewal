import psycopg2
from flask import Flask

from .. import config

# Get our config options
conf = config.config()

app = Flask(__name__)

# Set the config from conf.flask_confi
for item, value in list(conf.flask_config.items()):
    app.config[item] = value

conn = psycopg2.connect(
    dbname=conf.database["name"],
    user=conf.database["user"],
    host=conf.database["hostname"],
    password=conf.database["password"],
)

from . import views  # noqa F401 this makes the views available to WSGI
