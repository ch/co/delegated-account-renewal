#!/usr/bin/env python

from wtforms import (
    Form,
    SelectMultipleField,
    StringField,
    TextAreaField,
    validators,
    widgets,
)

# This works nicely in some browsers
try:
    from wtforms.fields.html5 import DateField
except ImportError:
    from wtforms.fields import DateField

from wtforms_components import DateRange

from . import conf
from .utils import getdaterange


class ExtensionForm(Form):
    (min, max) = getdaterange(conf)
    crsid = StringField("crsid", validators=[validators.InputRequired()])
    ext_date = DateField(
        "Date",
        format="%Y-%m-%d",
        validators=[validators.InputRequired(), DateRange(min, max)],
    )
    reason = TextAreaField("Reason", validators=[validators.InputRequired()])


class DelegateListForm(Form):
    delegates = SelectMultipleField(
        "Delegates", option_widget=widgets.CheckboxInput(), widget=widgets.TableWidget()
    )


class DelegateAppointForm(Form):
    app_delegate = StringField("crsid", validators=[validators.InputRequired()])
