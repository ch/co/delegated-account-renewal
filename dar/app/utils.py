#!/usr/bin/env python
import datetime
import re

import chemaccmgmt
import psycopg2
from flask import request

from . import conf, conn


def check_crsid(crsid):
    """Check if a crsid looks valid
    Args:
        crsid (str): the crsid

    Returns:
        (bool): True if valid
    """
    return re.match(r"^[a-z]{1,5}\d{0,5}$", crsid.lower()) is not None


class CommonDB(object):
    """Provide common database methods."""

    def __init__(self, dbconn=conn):
        self.cursor = dbconn.cursor()

    def _db_execute(self, query, arg_tuple):
        """Execute the db query.
        Args:
            query str: the query to execute
            arg_tuple tuple: the tuple to pass to the query.
        """
        if not isinstance(arg_tuple, tuple):
            # May get a string
            # Turn this to the tuple (var,)
            arg_tuple = (arg_tuple,)

        try:
            self.cursor.execute(query, arg_tuple)
            conn.commit()
            return True
        except psycopg2.ProgrammingError:
            conn.rollback()
            return False


class User(CommonDB):
    def __init__(self, username, dbconn=conn):
        super(User, self).__init__(dbconn)
        self.username = username
        self.crsid = get_crsid_from_username(username)
        self.is_valid = self.is_crsid_valid()

    def is_crsid_valid(self):
        # TODO need better crsid match
        if check_crsid(self.crsid):
            query = """SELECT COUNT(*)
            FROM {0}
            WHERE crsid = %s;
            """.format(
                conf.db_views["allowed_crsids"]
            )
            self._db_execute(query, self.crsid)
            return self.cursor.fetchone()[0] == 1
        else:
            return False


class PiUser(User):
    def __init__(self, username):
        super(PiUser, self).__init__(username)
        self.is_pi = self.check_is_pi()
        self.is_delegate = self.check_is_delegate()
        self.delegate = self.delegate_of()
        self.set_delegates()
        self.userlists = []
        self.all_userlist = []
        self._set_userlists()

    def check_is_pi(self):
        if self.is_valid:
            query = """SELECT COUNT(pi_crsid)
            FROM {0}
            WHERE pi_crsid = %s;
            """.format(
                conf.db_views["pi_delegate"]
            )
            self._db_execute(query, self.crsid)
            return self.cursor.fetchone()[0] > 0
        return False

    def check_is_delegate(self):
        if self.is_valid:
            query = """SELECT COUNT(delegate_crsid)
            FROM {0}
            WHERE delegate_crsid = %s;
            """.format(
                conf.db_views["pi_delegate"]
            )
            self._db_execute(query, self.crsid)
            return self.cursor.fetchone()[0] > 0
        return False

    def delegate_of(self):
        """Find who crsid is a delegate of"""
        if self.is_valid and self.is_delegate:
            query = """SELECT pi_crsid
            FROM {0}
            WHERE delegate_crsid = %s;
            """.format(
                conf.db_views["pi_delegate"]
            )
            self._db_execute(query, self.crsid)

            # Return the list of crsids
            return [i[0] for i in self.cursor.fetchall()]
        else:
            return None

    def get_delegates(self):
        if self.is_pi:
            query = """SELECT delegate_crsid, delegate_name
            FROM {0}
            WHERE pi_crsid = %s;
            """.format(
                conf.db_views["pi_delegate"]
            )
            self._db_execute(query, self.crsid)
            return [i[:2] for i in self.cursor.fetchall() if i[0] is not None]
        else:
            return []

    def set_delegates(self):
        self.delegate_list = self.get_delegates()
        self.delegate_crsid_list = [item[0] for item in self.delegate_list if item[0]]

    def appoint_delegate(self, delegate_crsid):
        delegate_user = User(delegate_crsid)
        if (
            self.is_pi
            and delegate_user.is_valid
            and delegate_crsid not in self.delegate_crsid_list
        ):
            query = """INSERT INTO {0}
                    (pi_crsid, delegate_crsid)
                    VALUES (%s, %s);
                    """.format(
                conf.db_views["pi_delegate"]
            )
            return self._db_execute(query, (self.crsid, delegate_crsid))
        else:
            return False

    def remove_delegate(self, delegate_crsid):
        delegate_user = User(delegate_crsid)
        if (
            self.is_pi
            and delegate_user.is_valid
            and delegate_crsid in self.delegate_crsid_list
        ):
            query = """DELETE FROM {0}
            WHERE
            pi_crsid = %s AND delegate_crsid = %s;
            """.format(
                conf.db_views["pi_delegate"]
            )
            return self._db_execute(query, (self.crsid, delegate_crsid))
        return False

    def do_extension(self, ext_crsid, reason, ext_date):
        ext_user = ExtUser(ext_crsid)
        if (self.is_pi or self.is_delegate) and ext_user.is_valid:
            query = """INSERT INTO {0}
            (ext_crsid, user_crsid, ext_date, reason)
            VALUES
            (%s, %s, %s, %s);
            """.format(
                conf.db_views["extension_details"]
            )
            adrun = ext_user.set_ad_expiry(ext_date)
            dbrun = self._db_execute(query, (ext_crsid, self.crsid, ext_date, reason))
            return all([adrun, dbrun])
        return False

    def _set_userlists(self):
        if self.is_pi:
            self.userlists.append(UserList(self.crsid))
        if self.is_delegate:
            for pi in self.delegate:
                self.userlists.append(UserList(pi))

        # Make these into a nice list of crsids
        for userlist in self.userlists:
            self.all_userlist += userlist.all_userlist


class ExtUser(User):
    def __init__(self, username):
        super(ExtUser, self).__init__(username)
        self.get_info()

    def get_info(self):
        query = """SELECT ext_email, ext_name, pi_crsid, pi_email, pi_name
        FROM {0}
        WHERE ext_crsid = %s;
        """.format(
            conf.db_views["extension_details"]
        )
        self._db_execute(query, self.crsid)
        (
            self.ext_email,
            self.ext_name,
            self.pi_crsid,
            self.pi_email,
            self.pi_name,
        ) = self.cursor.fetchone()

    def set_ad_expiry(self, expiry_date):
        """Set the expiry for the user.
        Args:
            expiry_date datetime.date: the date to set the expiry to.
        """
        if hasattr(conf, "debuguser"):
            crsid = conf.debuguser
        else:
            crsid = self.crsid

        try:
            (dn, pw) = chemaccmgmt.ui.get_creds()
            ad_conn = chemaccmgmt.ADConnect.connect_non_anon(dn=dn, password=pw)
        except EOFError:
            return False
        user = chemaccmgmt.ADUserAccount(crsid, connect=ad_conn)
        if not user.is_disabled(connect=ad_conn, quiet=True):
            success = user.set_expiry_date(
                expiry_date=expiry_date, connect=ad_conn, quiet=True
            )
        else:
            # Account is disabled.
            success = False
        return success


class UserList(CommonDB):
    """List of users that a pi can extend"""

    def __init__(self, pi_crsid, dbconn=conn):
        super(UserList, self).__init__(dbconn)
        self.pi_crsid = pi_crsid
        self.all_userlist = self.get_userlist()

    def get_userlist(self):
        query = """SELECT DISTINCT ON (ext_crsid) ext_crsid
        FROM {0}
        WHERE pi_crsid = %s;
        """.format(
            conf.db_views["extension_details"]
        )
        self._db_execute(query, self.pi_crsid)
        return [i[0] for i in self.cursor.fetchall()]

    @staticmethod
    def _get_expiry(crsid):
        """If an expiry date for crsid is set, return that, else return None."""
        try:
            user = chemaccmgmt.ADUserAccount(crsid).get_expiry_date()
        except chemaccmgmt.ad.NoSuchUserError:
            user = None
        return user

    def get_expire_users(self):
        """Get a list of users who have expiry dates set in the AD."""
        userlist = []
        if self.all_userlist:
            for user in self.all_userlist:
                expiry = self._get_expiry(user)
                if expiry:
                    userlist.append([user, expiry])
        return sorted(userlist, key=lambda x: x[1], reverse=True)


def get_crsid_from_username(username):
    """
    Get the crsid from the username
    """
    crsid = ""
    if username:
        if username.endswith("@cam.ac.uk"):
            # Raven Oauth2 UPN from which we can get identity
            # on which the app will do authz later
            crsid = username[: -len("@cam.ac.uk")]
        elif "@" in username:
            # Raven Oauth2 and definitely not an allowed UPN
            crsid = ""
        else:
            # Ucam-WebAuth
            crsid = username
    return crsid


def get_remote_user(req):
    """
    Get the REMOTE_USER from the web server.

    Args:header return to start
        req (request): Flask Request
    """
    if not hasattr(conf, "masquerade"):
        username = request.environ.get("REMOTE_USER", "")
    else:
        username = conf.masquerade
    return username


def getdaterange(conf):
    window = int(conf.extension_rules["window"])
    min_date = datetime.date.today()
    max_date = min_date + datetime.timedelta(days=window)
    return (min_date, max_date)
