from flask import (
    flash,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
)

from . import app, conf, forms, utils


@app.context_processor
def inject_debug():
    return dict(debug=app.debug)


@app.route("/")
@app.route("/index")
def index():
    user = utils.get_remote_user(req=request)
    pi_user = utils.PiUser(user)
    return render_template(
        "index.j2", templatestrings=conf.templatestrings, user=pi_user
    )


@app.route("/pl/<path:path>")
def send_pl(path):
    # This is for testing. /var/www/pl probably exists on the server.
    return send_from_directory("project-light-minimal/pl", path)


@app.route("/delegates", methods=("GET", "POST"))
def delegates():
    user = utils.get_remote_user(req=request)
    pi_user = utils.PiUser(user)

    # List of delegates
    delegatelistform = forms.DelegateListForm(request.form)
    delegatelistform.delegates.choices = pi_user.delegate_list

    appdelegateform = forms.DelegateAppointForm(request.form)

    if request.method == "POST":
        if "appoint" in request.form and appdelegateform.validate():
            delegate_crsid = request.form["app_delegate"].strip()
            if not utils.check_crsid(delegate_crsid):
                flash(
                    (
                        '"<b>{0}</b>" doesn\'t appear to be a valid crsid. '
                        "Please check and try again. {1}"
                    ).format(delegate_crsid, conf.templatestrings["assistance"]),
                    "alert",
                )
            elif delegate_crsid in pi_user.delegate_crsid_list:
                flash(
                    "Can't appoint {}. They already appear to be a delegate.".format(
                        delegate_crsid
                    ),
                    "alert",
                )
            else:
                if pi_user.appoint_delegate(delegate_crsid):
                    flash("Added delegate " + delegate_crsid, "success")
                    # renew the list of delegates
                    pi_user.set_delegates()
                    delegatelistform.delegates.choices = pi_user.delegate_list
                else:
                    flash("Unable to appoint delegate %s" % delegate_crsid, "alert")

        if "remove" in request.form:
            if request.form["delegates"]:
                rem_delegates = request.form.getlist("delegates")
                for user in rem_delegates:
                    if pi_user.remove_delegate(user):
                        flash(
                            "Removed delegate %s"
                            % dict(pi_user.delegate_list).get(user, user),
                            "success",
                        )
                    else:
                        flash(
                            "Failed to remove delegate %s"
                            % dict(pi_user.delegate_list).get(user, user),
                            "alert",
                        )
                # renew the list of delegates
                pi_user.set_delegates()
                delegatelistform.delegates.choices = pi_user.delegate_list

    return render_template(
        "delegates.j2",
        delegatelistform=delegatelistform,
        appdelegateform=appdelegateform,
        templatestrings=conf.templatestrings,
    )


@app.route("/extend", methods=("GET", "POST"))
def extend():
    user = utils.get_remote_user(req=request)
    pi_user = utils.PiUser(user)
    form = forms.ExtensionForm(request.form)

    if request.method == "POST" and form.validate():
        ext_crsid = form.crsid.data.strip()
        if not utils.check_crsid(ext_crsid):
            flash(
                (
                    '"<b>{0}</b>" doesn\'t appear to be a valid crsid. '
                    "Please check and try again. {1}"
                ).format(ext_crsid, conf.templatestrings["assistance"]),
                "alert",
            )
        elif ext_crsid in pi_user.all_userlist:
            if pi_user.do_extension(ext_crsid, form.reason.data, form.ext_date.data):
                flash(
                    "Extending expiry date for {0} to {1}".format(
                        ext_crsid, form.ext_date.data
                    ),
                    "success",
                )
                # clear form, redirect here
                form = forms.ExtensionForm()
                redirect(url_for("extend"))
            else:
                flash(
                    "Failed to extend expiry date for {0}. {1}".format(
                        ext_crsid, conf.templatestrings["assistance"]
                    ),
                    "alert",
                )
        else:
            flash(
                (
                    "The user {0} does not appear to be a user "
                    "you are allowed to extend. {1}"
                ).format(ext_crsid, conf.templatestrings["assistance"]),
                "alert",
            )
    return render_template(
        "extenduser.j2",
        user=pi_user,
        form=form,
        templatestrings=conf.templatestrings,
        extension_rules=conf.extension_rules,
    )
