#!/usr/bin/env python

import os

import yaml

basedir = os.path.abspath(os.path.dirname(__file__))
config_filename = "config.yml"
search_paths = [
    os.path.join(os.sep, "etc", "delegated-extension", config_filename),
    os.path.join(basedir, config_filename),
]


class config:
    def __init__(self):
        configfile = self._get_first_config_found(search_paths)
        self._setconfig(self._getconfig(configfile))

    def _get_first_config_found(self, search_paths):
        for fname in search_paths:
            if os.path.exists(fname):
                return fname
        return False

    def _getconfig(self, conffilename):
        if conffilename:
            with open(conffilename, "r") as conffile:
                config = yaml.safe_load(conffile)
            return config

    def _setconfig(self, config):
        for item in list(config.keys()):
            setattr(self, item, config[item])
