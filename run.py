#!/usr/bin/env python3
import argparse
import os

import dar.app

p = argparse.ArgumentParser(description="Run DAR app in test mode")
p.add_argument("pi_username", help="PI username to fake", nargs="?")
p.add_argument("student_username", help="Student username to fake", nargs="?")
args = p.parse_args()

if "pi_username" in args:
    dar.app.conf.masquerade = args.pi_username
if "student_username" in args:
    dar.app.conf.debuguser = args.student_username

if "FLASK_RUN_HOST" in os.environ:
    dar.app.app.run(host=os.environ["FLASK_RUN_HOST"])
else:
    dar.app.app.run()
